<?php 
require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "Nama :" . $sheep->name . "<br>"; // "shaun"
echo "berkaki :" . $sheep->legs . "<br>"; // 2
echo "berdarah dingin :" . $sheep->cold_blooded . "<br><br>"; // false

$frog = new frog("buduk");
echo "Nama :" . $frog->name . "<br>";
echo "berkaki :" . $frog->legs . "<br>"; // 2
$frog->jump(); // "hop hop"
echo "<br><br>" ;

$ape = new ape("kera sakti");
echo "Nama :" . $ape->name . "<br>"; // 
echo "berkaki :" . $ape->legs . "<br>"; // 2
echo $ape->yell() . "<br><br>" ; // "Auooo"


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>